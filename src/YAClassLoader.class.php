﻿<?php
/**
 * Yet Another Class Loader
 * Author: Darren Hamilton
 */
class YAClassLoader {

   static $YAClassLoader;
   
   public $preloadClasses;
   public $foundClasses;
   
   public $filename;
   
   static $lastPreloadClasses = Array();
   static $lastFoundClasses = Array();
   

   public function __construct() {
      $this->filename = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'obj' . DIRECTORY_SEPARATOR . 'YAClassLoader.obj.php';

      if(file_exists($this->filename)) {
         $fc = file_get_contents($this->filename);
         $obj = unserialize($fc);
         $this->preloadClasses = $obj->preloadClasses;
         $this->foundClasses = $obj->foundClasses;
      } else {
         $this->foundClasses = Array();
         $this->preloadClasses = Array();
      }
      
      YAClassLoader::$lastPreloadClasses = $this->preloadClasses;
      YAClassLoader::$lastFoundClasses = $this->foundClasses;
   }
   
   public function __destruct() {

      if(count(array_diff($this->foundClasses, YAClassLoader::$lastFoundClasses)) != 0) {
         $fc = serialize($this);
         file_put_contents($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'obj' . DIRECTORY_SEPARATOR . 'YAClassLoader.obj.php', $fc);
      }
   }
   
   public function addPreloadClass($folder, $className, $file) {
      if(isset($this->preloadClasses[$folder])) {
         $this->preloadClasses[$folder][$className] = $file;
      } else {
         $this->preloadClasses[$folder] = Array();
         $this->preloadClasses[$folder][$className] = $file;
      }
   }
  
   /**
    * This function recursively includes all classes within a folder.
    * If $load is set to true, it also instantiates one instance of the class.
    * therefore any class that is intended to be loaded must have a constructor with
    * no arguments.
    * If $load is false the class will be made available to the __autoload function.
    */
   static function loadFolder($folder, $load = false, $originalFolder = '') {

      if($originalFolder === '') {
         $originalFolder = $folder;
      }
      
      if(isset(YAClassLoader::$YAClassLoader->preloadClasses[$folder])) {
         
         if(!$load) {
            return true;
         }
      
         $classes = YAClassLoader::$YAClassLoader->preloadClasses[$folder];
           
         foreach($classes as $className => $file) {
              
            if(file_exits($file)) {
               include_once($file);
               new $className();
            }                  
         }
            
         return true;
      }  
      
      $dirs = scandir($folder);
      
      foreach($dirs as $row => $file) {
      
         if($file != '.' && $file != '..') {
            if(is_dir($folder . '/' . $file)) {
            // If the current file is a folder, iterate over it. If it returns true the class has been found,
            // True can be returned here to mark the end of the search as because levels up of here will also return true.
               YAClassLoader::loadFolder($folder . '/' . $file, $load, $originalFolder);
            } else {

               // If the name of the current file ends in the .class.php or .php, include it.
			      if(strpos($file, '.class.php') > 0) {
                  YAClassLoader::$YAClassLoader->foundClasses[$file] = $folder . '/' . $file; 
                  $className = (substr($file, 0, strlen('.class.php') * -1));               
                  YAClassLoader::$YAClassLoader->addPreloadClass($originalFolder, $className, $folder . '/' . $file);
                  
                  if($load == true) {
                     include_once($folder . '/' . $file);
                     new $className();
                  }
               } else if(strpos($file, '.class') == 0 && strpos($file, '.php') > 0 ) {
                  YAClassLoader::$YAClassLoader->foundClasses[$file] = $folder . '/' . $file;
                  $className = (substr($file, 0, strlen('.php') * -1));
                  YAClassLoader::$YAClassLoader->addPreloadClass($originalFolder, $className, $folder . '/' . $file);               
                  if($load == true) {
                     include_once($folder . '/' . $file);
                     new $className();
                  }
               }
            }
         }
      }
   }
}

   /**
    * Auto Load is automatically called when a class is accessed that has not yet been declared.
    * This looks for the class by iterating over all directories within the classes folder until it is found.
    * Additional folders can be loaded with the loadFolder method.
    * For a class to be found by this function, the file must be in the following format:
    * {__CLASS_NAME__}.class.php or {__CLASS_NAME__}.php 
    */
   function __autoload($class_name) {
      if(isset(YAClassLoader::$YAClassLoader->foundClasses[$class_name . '.class.php'])) {
         include(YAClassLoader::$YAClassLoader->foundClasses[$class_name . '.class.php']);
         return true;
      }
      
      if(isset(YAClassLoader::$YAClassLoader->foundClasses[$class_name . '.php'])) {
         include(YAClassLoader::$YAClassLoader->foundClasses[$class_name . '.php']);
         return true;
      }
      
      if(scan($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'classes', $class_name . '.class.php') == false) {
         if(scan($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'classes', $class_name . '.php') == false) {
            return false;
         }
         
         return true;
      }
      
      return true;
   }
   
   function scan($folder, $class) {
   
      $dirs = scandir($folder);

      foreach($dirs as $row => $file) {

         if($file != '.' && $file != '..') {
            if(is_dir($folder . '/' . $file)) {
           
               // If the current file is a folder, iterate over it. If it returns true the class has been found,
               // True can be returned here to mark the end of the search as because levels up of here will also return true.
               if(scan($folder . '/' . $file, $class) === true) {
                  return true;
               }

            } else {

               // If the name of the current file matches the name of the class then a match has been found and can be included.
               // If it is not a match, the search can continue.
               if($file == $class) {
                  include_once($folder . '/' . $file);
                  YAClassLoader::$YAClassLoader->foundClasses[$file] = $folder . '/' . $file;
                  return true;
               }
            }
         }
      }
   }
   
   ini_set('unserialize_callback_func', '__autoload');
   
   YAClassLoader::$YAClassLoader = new YAClassLoader();